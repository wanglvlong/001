



import VueRouter from 'vue-router'
//导入template组件
import home from './components/tabber/HomeContainer.vue'
import members from './components/tabber/MemberContainer.vue'
import shop from './components/tabber/ShopContainer.vue'
import search from './components/tabber/SearchContainer.vue'







var router = new VueRouter({
    routes: [
        {path:'/home',component:home},
        {path:'/members',component:members},
        {path:'/shop',component:shop},
        {path:'/search',component:search}

    ],
    linkActiveClass:'mui-active'

})


export default router