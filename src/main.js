// console.log('ok')

// 入口文件

import Vue from 'vue'

//导入mui的样式
import '../src/lib/mui/css/mui.min.css'
//导入购物车样式
import '../src/lib/mui/css/icons-extra.css'

Vue.config.devtools = true

//导入vue-router包
import VueRouter from 'vue-router'
//手动安装router包
Vue.use(VueRouter)

//导入自己的router模块
import router from './router.js'




//按需导入mint-ui中的组件
// import {Header} from 'mint-ui'

// Vue.component(Header.name,Header)
// import { Header } from 'mint-ui';

// Vue.component(Header.name, Header);

// import { Button } from 'mint-ui';

// Vue.component(Button.name, Button);

//完整引入
import MintUI from 'mint-ui';
import 'mint-ui/lib/style.css'
Vue.use(MintUI);

//按需引入

// import { Header } from 'mint-ui'
// Vue.component(Header.name, Header)

import app from './App.vue'



//导入轮播图

import { Swipe, SwipeItem } from 'mint-ui';

Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);


//导入vue-resourse
import VueResource from 'vue-resource'
Vue.use(VueResource)


var vm =new Vue({
    el:'#app',
    render:c=>c(app),
    router,
    
})